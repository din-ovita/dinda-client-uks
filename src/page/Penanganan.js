import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Penanganan() {
  const [penanganan, setPenanganan] = useState([]);
  const [namaPenanganan, setNamaPenanganan] = useState([]);
  // handle modal
  const [show, setShow] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  // const post penanganan
  const addPenanganan = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:2929/penanganan", {
        namaPenanganan: namaPenanganan,
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan Penanganan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };

  // const get all penanganan
  const getAllPenanganan = async () => {
    await axios
      .get("http://localhost:2929/penanganan/all-penanganan")
      .then((res) => {
        setPenanganan(res.data.data);
      });
  };

  // const delete penanganan
  const delPenanganan = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:2929/penanganan/" + id).catch(() => {
          Swal.fire({
            icon: "error",
            title: "Error..",
            text: "Penanganan pertama tidak bisa dihapus!",
            showConfirmButton: false,
          });
        });
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };

  // untuk menampung data
  useEffect(() => {
    getAllPenanganan();
  }, []);

  return (
    <>
      <div className="pt-[5rem] pr-[1rem] pb-[3rem] pl-[19rem]">
        <div className="shadow-lg rounded-lg my-8 mx-[20px]">
          {/* header */}
          <div className="flex justify-between items-center bg-[#00dfc4] rounded-t-lg px-5 py-3">
            <h5 className="text-2xl text-white">Penanganan Pertama</h5>
            <Button variant="primary" onClick={handleShow}>
              Tambah
            </Button>
          </div>
          {/* end header */}
          {/* table */}
          <div className="pt-3 pb-10 px-2">
            <table className="my-[2rem] mx-[3rem] w-[90%]">
              <thead className="text-[15px] uppercase text-center bg-gray-200">
                <tr>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    No
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Nama Penanganan
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Aksi
                  </th>
                </tr>
              </thead>
              <tbody className="text-center text-[14px]">
                {penanganan.length === 0 ? (
                  <>
                    {" "}
                    <tr>
                      <td
                        colspan="3"
                        className="border-b border-b-gray-200 px-6 py-3 whitespace-nowrap text-base text-center font-medium text-gray-900"
                      >
                        Tidak ada data
                      </td>
                    </tr>
                  </>
                ) : (
                  <>
                    {" "}
                    {penanganan.map((penanganan1, index) => (
                      <tr className="border-b border-b-gray-200">
                        <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {index + 1}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {penanganan1.namaPenanganan}
                        </td>
                        <td className="px-6 whitespace-nowrap font-medium text-center">
                          <a
                            href={"/editPenanganan/" + penanganan1.id}
                            className="text-green-500 hover:text-green-600 text-[20px] mr-5"
                          >
                            <i class="fa-solid fa-pen-to-square"></i>
                          </a>
                          <button
                            className="text-red-500 hover:text-red-600 text-[20px]"
                            onClick={() => delPenanganan(penanganan1.id)}
                          >
                            <i class="fa-solid fa-trash"></i>
                          </button>
                        </td>
                      </tr>
                    ))}
                  </>
                )}
              </tbody>
            </table>
          </div>
          {/* end table */}
        </div>
      </div>
      {/* modal */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Tambah Penanganan</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {" "}
          <form onSubmit={addPenanganan} className="px-6">
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Penanganan
              </label>
              <input
                placeholder="Nama Penanganan"
                onChange={(e) => setNamaPenanganan(e.target.value)}
                value={namaPenanganan}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                required
              />
            </div>
            <div className="flex gap-4 mt-4 ml-[9rem]">
              <button
                onClick={handleClose}
                className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Batal
              </button>
              <button
                type="submit"
                className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Tambah
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      {/* end modal */}
    </>
  );
}
