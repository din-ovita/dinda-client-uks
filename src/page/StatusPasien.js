import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function StatusPasien() {
  const [pasien, setPasien] = useState({
    data: {},
  });
  const [penyakit, setPenyakit] = useState([]);
  const [penanganan, setPenanganan] = useState([]);
  const [tindakan, setTindakan] = useState([]);
  const [obat, setObat] = useState([]);
  const [statusPeriksa, setStatusPeriksa] = useState([]);

  const [diagnosa, setDiagnosa] = useState(0);
  const [namaTindakan, setNamaTindakan] = useState(0);
  const [namaPenanganan, setNamaPenanganan] = useState(0);
  const [namaObat, setNamaObat] = useState(0);

  // mengambil id
  const param = useParams();
  const history = useHistory();
  // add
  const add = async (e) => {
    e.preventDefault();
    try {
      await axios.post(
        `http://localhost:2929/status-periksa`,
        {
          pasienId: param.id,
          obatId: namaObat,
          diagnosaId: diagnosa,
          penangananId: namaPenanganan,
          tindakanId: namaTindakan,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      await axios.put(`http://localhost:2929/pasien/periksa/` + param.id, {
        penanganan: "Sudah ditangani",
      });
      Swal.fire({
        icon: "success",
        title: "Sukses Periksa Pasien",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/periksa-pasien");
        window.location.reload();
      }, 1000);
    } catch (err) {
      console.log(err);
    }
  };

  // put
  // const put = async (e) => {
  //   e.preventDefault();
  //   try {
  //     await axios.put(`http://localhost:2929/pasien/periksa/` + param.id, {
  //       penanganan: "Sudah ditangani",
  //     });
  //     Swal.fire({
  //       icon: "success",
  //       title: "Berhasil Mengedit",
  //       showConfirmButton: false,
  //       timer: 1500,
  //     });
  //     setTimeout(() => {
  //       history.push("/periksa-pasien");
  //       window.location.reload();
  //     }, 1500);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  // get all status periksa
  const allStatusPeriksa = async () => {
    const status = await axios.get(
      `http://localhost:2929/status-periksa/by-pasien?pasien_id=` + param.id,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    setStatusPeriksa(status.data.data);
  };

  // get all penyakit
  const allPenyakit = async () => {
    const diagnosa = await axios.get(
      `http://localhost:2929/diagnosa/all-diagnosa`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    setPenyakit(diagnosa.data.data);
  };

  // get all penanganan
  const allPenanganan = async () => {
    const penanganan = await axios.get(
      `http://localhost:2929/penanganan/all-penanganan`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    setPenanganan(penanganan.data.data);
  };

  // get all tindakan
  const allTindakan = async () => {
    const tindakan = await axios.get(
      `http://localhost:2929/tindakan/all-tindakan`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    setTindakan(tindakan.data.data);
  };

  // get all obat
  const allObat = async () => {
    const obat = await axios.get(`http://localhost:2929/obat/all-obat`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    setObat(obat.data.data);
  };

  // untuk menampung data
  useEffect(() => {
    allPenyakit();
    allObat();
    allPenanganan();
    allTindakan();
    allStatusPeriksa();
  }, []);

  // menampung data
  useEffect(() => {
    axios
      .get("http://localhost:2929/pasien/" + param.id)
      .then((response) => {
        setPasien(response.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);

  console.log(pasien);
  console.log(statusPeriksa);

  return (
    <div className="pt-[4rem] pr-[1rem] pb-[3rem] pl-[19rem]">
      <div className="shadow-lg rounded-lg my-8 mx-[20px]">
        {/* header */}
        <h5 className="bg-[#00dfc4] rounded-t-lg px-5 py-3 text-center text-white">
          Periksa Pasien : {pasien.data.nama}
        </h5>
        {/* end header */}
        <div className="flex flex-col p-4">
          {/* card pasien */}
          <div className="flex flex-row gap-[20px]">
            <div>
              <h1 className="text-sm font-semibold">Nama Pasien</h1>
              <p className="bg-gray-100 rounded-md border border-gray-300 py-2 px-3 text-sm w-[28rem]">
                {pasien.data.nama}
              </p>
            </div>
            <div>
              <h1 className="text-sm font-semibold">Status Pasien</h1>
              <p className="capitalize bg-gray-100 rounded-md border border-gray-280 py-2 px-3 text-sm w-[30rem]">
                {pasien.data.status}
              </p>
            </div>
          </div>
          <div>
            <h1 className="text-sm font-semibold">Keluhan</h1>
            <p className="bg-gray-100 border rounded-md border-gray-300 py-2 px-3 text-sm h-[4rem]">
              {pasien.keluhan}
            </p>
          </div>
          {/* end card pasien */}
          {/* form penanganan */}
          <form onSubmit={add}>
            <div className="flex flex-row gap-[31px]">
              <div>
                <label className="text-sm font-semibold">Penyakit Pasien</label>
                <select
                  className="bg-gray-100 rounded-md border border-gray-300 py-2 px-2 text-sm w-[13.2rem]"
                  onChange={(e) => setDiagnosa(e.target.value)}
                >
                  <option selected>Pilih Penyakit : </option>
                  {penyakit.map((penyakit1) => (
                    <option value={penyakit1.id}>
                      {penyakit1.namaDiagnosa}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <label className="text-sm font-semibold">
                  Penanganan Pertama
                </label>
                <select
                  className="bg-gray-100 rounded-md border border-gray-300 py-2 px-2 text-sm w-[13.2rem]"
                  onChange={(e) => setNamaPenanganan(e.target.value)}
                >
                  <option selected>Pilih Penanganan : </option>
                  {penanganan.map((penanganan1) => (
                    <option value={penanganan1.id}>
                      {penanganan1.namaPenanganan}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <label className="text-sm font-semibold">Tindakan</label>
                <select
                  className="bg-gray-100 rounded-md border border-gray-300 py-2 px-2 text-sm w-[13.2rem]"
                  onChange={(e) => setNamaTindakan(e.target.value)}
                >
                  <option selected>Pilih Tindakan : </option>
                  {tindakan.map((tindakan1) => (
                    <option value={tindakan1.id}>
                      {tindakan1.namaTindakan}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <label className="text-sm font-semibold">Obat</label>
                <select
                  className="bg-gray-100 rounded-md border border-gray-300 py-2 px-2 text-sm w-[13.2rem]"
                  onChange={(e) => setNamaObat(e.target.value)}
                >
                  <option selected>Pilih Obat : </option>
                  {obat.map((obat1) => (
                    <option value={obat1.id}>{obat1.namaObat}</option>
                  ))}
                </select>
              </div>
            </div>
            <div className="flex justify-end py-4">
              <Button variant="success" type="submit">
                Selesai
              </Button>
            </div>
          </form>
          {/* end form penanganan */}
        </div>
      </div>
    </div>
  );
}
