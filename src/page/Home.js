import React from "react";

function Home() {
  return (
    <div className="home flex justify-center items-center py-[9rem]">
      <div className="tab shadow-lg rounded-lg flex justify-center items-center flex-col px-[20rem]">
        <h1 className="text-center pt-10 font-semibold text-[#00dfc4]">
          Selamat Datang <br /> <span>Di Sistem Aplikasi UKS</span>
        </h1>
        <p className="text-center text-xl pt-2 text-[#292929]">
          SMP NEGERI 1 SEMARANG
        </p>
        <p className="text-center text-lg pt-2 text-gray-500">
          Sistem untuk memudahkan program Unit Kesehatan Sekolah
        </p>
        {/* menuju page dashboard */}
        <a
          href="/dashboard"
          className="text-center mt-3 mb-10 no-underline text-sm text-white tracking-wider bg-[#00dfc4] hover:bg-[#12a392] duration-200 py-3 px-4"
        >
          Klik untuk Melanjutkan
        </a>
      </div>
    </div>
  );
}

export default Home;
