import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Guru() {
  const [guru, setGuru] = useState([]);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [tglLahir, setTglLahir] = useState("");
  // excell
  const [excel, setExcel] = useState("");
  // handle modal
  const [show, setShow] = useState(false);
  const [showExcel, setShowExcel] = useState(false);

  // handle modal
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const handleShowExcel = () => setShowExcel(true);
  const handleCloseExcel = () => setShowExcel(false);

  // import data guru / const post data guru by excel
  const importGuruFromExcel = async (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("file", excel);

    await axios
      .post(`http://localhost:2929/data/api/excel/upload/data`, formData)
      .then(() => {
        handleCloseExcel();
        getAllGuru();
        Swal.fire("Sukses!", " Berhasil Ditambahkan.", "success");
      })
      .catch((err) => {
        console.log(err);
        Swal.fire("Error", "Anda belum memilih file untuk diimport!.", "error");
      });
  };

  // download format data guru
  const downloadFormatDataGuru = async () => {
    await Swal.fire({
      title: "Apakah Anda Ingin Mendownload?",
      text: "Ini file format excel untuk mengimport data",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0b409c",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:2929/data/api/excel/download/format-data`,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          setTimeout(() => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement("a");

            fileLink.href = fileURL;
            fileLink.setAttribute("download", "contoh-format-data.xlsx");
            document.body.appendChild(fileLink);

            fileLink.click();
          }, 2000);
        });
      }
    });
  };

  // download data guru
  const downloadDataGuru = async () => {
    await Swal.fire({
      title: "Apakah Anda Ingin Mendownload?",
      text: "File berisi semua data guru",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0b409c",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:2929/data/api/excel/download/data?status=guru`,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          setTimeout(() => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement("a");

            fileLink.href = fileURL;
            fileLink.setAttribute("download", "data.xlsx");
            document.body.appendChild(fileLink);

            fileLink.click();
          }, 2000);
        });
      }
    });
  };

  // const tambah data guru
  const addGuru = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:2929/data", {
        nama: nama,
        alamat: alamat,
        status: "guru",
        tempatLahir: tempatLahir,
        tglLahir: tglLahir,
        jabatan: "guru",
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan Data Guru",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };

  // get all data guru
  const getAllGuru = async () => {
    await axios
      .get("http://localhost:2929/data/all-by-status?status=guru")
      .then((res) => {
        setGuru(res.data.data);
      });
  };

  // delete guru
  const delGuru = async (id) => {
    try {
      Swal.fire({
        title: "Apakah Anda Ingin Menghapus?",
        text: "Perubahan data tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Hapus",
        cancelButtonText: "Batal",
      }).then((result) => {
        if (result.isConfirmed) {
          axios.delete("http://localhost:2929/data/" + id).catch(() => {
            Swal.fire({
              icon: "error",
              title: "Error..",
              text: "Data guru tidak bisa dihapus!",
              showConfirmButton: false,
            });
          });
          Swal.fire({
            icon: "success",
            title: "Dihapus!",
            showConfirmButton: false,
            timer: 1500,
          });
          setTimeout(() => {
            window.location.reload();
          }, 1500);
        }
      });
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Gagal!",
        text: "Guru tidak bisa di hapus karena sudah periksa",
      });
    }
  };

  // untuk menampung data
  useEffect(() => {
    getAllGuru();
  }, []);

  return (
    <>
      <div className="pt-[5rem] pr-[1rem] pb-[3rem] pl-[19rem]">
        <div className="shadow-lg rounded-lg my-8 mx-[20px]">
          {/* header */}
          <div className=" bg-[#00dfc4] rounded-t-lg px-5 py-3">
            <div className="flex justify-between items-center">
              <h5 className="text-2xl text-white">Data Guru</h5>
              <div className="flex gap-3">
                <Button variant="primary" onClick={handleShow} className="w-44">
                  Tambah
                </Button>
                <Button
                  variant="primary"
                  onClick={handleShowExcel}
                  className="w-44"
                >
                  Import Data
                </Button>
              </div>
            </div>{" "}
            <div className="flex justify-end pt-3">
              <Button
                variant="primary"
                onClick={downloadDataGuru}
                className="w-44"
              >
                Download Data
              </Button>
            </div>
          </div>
          {/* end header */}
          {/* table */}
          <div className="pt-3 pb-10 px-2">
            <table className="my-[2rem] mx-[3rem] w-[90%]">
              <thead className="text-[15px] uppercase text-center bg-gray-200">
                <tr>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    No
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Nama Guru
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Tempat, Tanggal Lahir{" "}
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Alamat{" "}
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Aksi
                  </th>
                </tr>
              </thead>
              <tbody className="text-center text-[14px]">
                {/* validasi jika data guru tidak ada */}
                {guru.length === 0 ? (
                  <>
                    {" "}
                    <tr>
                      <td
                        colspan="5"
                        className="border-b border-b-gray-200 px-6 py-3 whitespace-nowrap text-base text-center font-medium text-gray-900"
                      >
                        Tidak ada data
                      </td>
                    </tr>
                  </>
                ) : (
                  <>
                    {" "}
                    {guru.map((guru1, index) => (
                      <tr className="border-b border-b-gray-200">
                        <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {index + 1}
                        </td>
                        <td className="capitalize px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {guru1.nama}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {guru1.tempatLahir}, {guru1.tglLahir}
                        </td>
                        <td className="capitalize px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {guru1.alamat}
                        </td>
                        <td className="px-6 whitespace-nowrap font-medium text-center">
                          <a
                            href={"/editGuru/" + guru1.id}
                            className="text-green-500 hover:text-green-600 text-[20px] mr-5"
                          >
                            <i class="fa-solid fa-pen-to-square"></i>
                          </a>
                          <button
                            className="text-red-500 hover:text-red-600 text-[20px]"
                            onClick={() => delGuru(guru1.id)}
                          >
                            <i class="fa-solid fa-trash"></i>
                          </button>
                        </td>
                      </tr>
                    ))}
                  </>
                )}
              </tbody>
            </table>
          </div>
          {/* end table */}
        </div>
      </div>
      {/* modal post */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Tambah Data Guru</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {" "}
          <form onSubmit={addGuru} className="px-6">
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Nama Guru
              </label>
              <input
                placeholder="Nama Guru"
                onChange={(e) => setNama(e.target.value)}
                value={nama}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                required
              />
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Tempat Lahir{" "}
              </label>
              <input
                placeholder="Tempat Lahir"
                onChange={(e) => setTempatLahir(e.target.value)}
                value={tempatLahir}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                required
              />
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Tanggal Lahir{" "}
              </label>
              <input
                placeholder="Tanggal Lahir"
                type="date"
                onChange={(e) => setTglLahir(e.target.value)}
                value={tglLahir}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                required
              />
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Alamat{" "}
              </label>
              <input
                placeholder="Alamat"
                onChange={(e) => setAlamat(e.target.value)}
                value={alamat}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                required
              />
            </div>
            <div className="flex gap-4 mt-4 ml-[9rem]">
              <button
                onClick={handleClose}
                className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Batal
              </button>
              <button
                type="submit"
                className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Tambah
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      {/* end modal post */}
      {/* modal excel */}
      <Modal show={showExcel} onHide={handleCloseExcel}>
        <Modal.Header closeButton>
          <Modal.Title>Import Guru Dari File Excel</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="rounded-md text-center shadow-md py-3 border">
            <p>
              Download file di bawah ini untuk menginput data guru <br />{" "}
              <span className="font-semibold">
                (*column tanggal lahir diubah menjadi date*)
              </span>
            </p>
            <Button variant="primary" onClick={downloadFormatDataGuru}>
              Download Format File
            </Button>
          </div>
          <form className="mt-4" onSubmit={importGuruFromExcel}>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Drop File.xlsx
              </label>
              <input
                type="file"
                required
                accept=".xlsx"
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                onChange={(e) => setExcel(e.target.files[0])}
              />
            </div>
            <div className="flex gap-4 mt-4 ml-[9rem]">
              <button
                onClick={handleCloseExcel}
                className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Batal
              </button>
              <button
                type="submit"
                className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Tambah
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      {/* end modal excel */}
    </>
  );
}
