import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Siswa() {
  const [siswa, setSiswa] = useState([]);
  const [nama, setNama] = useState("");
  const [kelas, setKelas] = useState("");
  const [alamat, setAlamat] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [tglLahir, setTglLahir] = useState("");
  const [excel, setExcel] = useState("");
  // handle modal
  const [show, setShow] = useState(false);
  const [showExcel, setShowExcel] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const handleShowExcel = () => setShowExcel(true);
  const handleCloseExcel = () => setShowExcel(false);

  // import data guru / const post data guru by excel
  const importSiswaFromExcel = async (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("file", excel);

    await axios
      .post(`http://localhost:2929/data/api/excel/upload/data`, formData)
      .then(() => {
        handleCloseExcel();
        getAllsSiswa();
        Swal.fire("Sukses!", " Berhasil Ditambahkan.", "success");
      })
      .catch((err) => {
        console.log(err);
        Swal.fire("Error", "Anda belum memilih file untuk diimport!.", "error");
      });
  };

  // download format data siswa
  const downloadFormatDataSiswa = async () => {
    await Swal.fire({
      title: "Apakah Anda Ingin Mendownload?",
      text: "Ini file format excel untuk mengimport data",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0b409c",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:2929/data/api/excel/download/format-data`,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          setTimeout(() => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement("a");

            fileLink.href = fileURL;
            fileLink.setAttribute("download", "contoh-format-data.xlsx");
            document.body.appendChild(fileLink);

            fileLink.click();
          }, 2000);
        });
      }
    });
  };

  // dwonload data siswa
  const downloadDataSiswa = async () => {
    await Swal.fire({
      title: "Apakah Anda Ingin Mendownload?",
      text: "File berisi semua data siswa",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0b409c",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:2929/data/api/excel/download/data?status=siswa`,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          setTimeout(() => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement("a");

            fileLink.href = fileURL;
            fileLink.setAttribute("download", "data.xlsx");
            document.body.appendChild(fileLink);

            fileLink.click();
          }, 2000);
        });
      }
    });
  };

  // post siswa
  const addSiswa = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:2929/data", {
        nama: nama,
        alamat: alamat,
        status: "siswa",
        tempatLahir: tempatLahir,
        tglLahir: tglLahir,
        jabatan: kelas,
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan Data Siswa",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };

  // get all siswa
  const getAllsSiswa = async () => {
    await axios
      .get("http://localhost:2929/data/all-by-status?status=siswa")
      .then((res) => {
        setSiswa(res.data.data);
      });
  };

  // delete siswa
  const delSiswa = async (id) => {
    try {
      Swal.fire({
        title: "Apakah Anda Ingin Menghapus?",
        text: "Perubahan data tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Hapus",
        cancelButtonText: "Batal",
      }).then((result) => {
        if (result.isConfirmed) {
          axios.delete("http://localhost:2929/data/" + id).catch(() => {
            Swal.fire({
              icon: "error",
              title: "Error..",
              text: "Data siswa tidak bisa dihapus!",
              showConfirmButton: false,
            });
          });
          Swal.fire({
            icon: "success",
            title: "Dihapus!",
            showConfirmButton: false,
            timer: 1500,
          });
          setTimeout(() => {
            window.location.reload();
          }, 1500);
        }
      });
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Gagal!",
        text: "Siswa tidak bisa di hapus karena sudah periksa",
      });
    }
  };

  // untuk menampung data
  useEffect(() => {
    getAllsSiswa();
  }, []);

  return (
    <>
      <div className="pt-[5rem] pr-[1rem] pb-[3rem] pl-[19rem]">
        <div className="shadow-lg rounded-lg my-8 mx-[20px]">
          {/* header */}
          <div className=" bg-[#00dfc4] rounded-t-lg px-5 py-3">
            <div className="flex justify-between items-center">
              <h5 className="text-2xl text-white">Data Siswa</h5>
              <div className="flex gap-3">
                <Button variant="primary" onClick={handleShow} className="w-44">
                  Tambah
                </Button>
                <Button
                  variant="primary"
                  onClick={handleShowExcel}
                  className="w-44"
                >
                  Import Data
                </Button>
              </div>
            </div>{" "}
            <div className="flex justify-end pt-3">
              <Button
                variant="primary"
                onClick={downloadDataSiswa}
                className="w-44"
              >
                Download Data
              </Button>
            </div>
          </div>
          {/* end header */}
          {/* table */}
          <div className="pt-3 pb-10 px-2">
            <table className="my-[2rem] mx-[3rem] w-[90%]">
              <thead className="text-[15px] uppercase text-center bg-gray-200">
                <tr>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    No
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Nama Siswa
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Kelas
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Tempat, Tanggal Lahir{" "}
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Alamat{" "}
                  </th>
                  <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Aksi
                  </th>
                </tr>
              </thead>
              <tbody className="text-center text-[14px]">
                {siswa.length === 0 ? (
                  <>
                    {" "}
                    <tr>
                      <td
                        colspan="6"
                        className="border-b border-b-gray-200 px-6 py-3 whitespace-nowrap text-base text-center font-medium text-gray-900"
                      >
                        Tidak ada data
                      </td>
                    </tr>
                  </>
                ) : (
                  <>
                    {" "}
                    {siswa.map((siswa1, index) => (
                      <tr className="border-b border-b-gray-200">
                        <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {index + 1}
                        </td>
                        <td className="capitalize px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {siswa1.nama}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {siswa1.jabatan}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {siswa1.tempatLahir}, {siswa1.tglLahir}
                        </td>
                        <td className="capitalize px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {siswa1.alamat}
                        </td>
                        <td className="px-6 whitespace-nowrap font-medium text-center">
                          <a
                            href={"/editSiswa/" + siswa1.id}
                            className="text-green-500 hover:text-green-600 text-[20px] mr-5"
                          >
                            <i class="fa-solid fa-pen-to-square"></i>
                          </a>
                          <button
                            className="text-red-500 hover:text-red-600 text-[20px]"
                            onClick={() => delSiswa(siswa1.id)}
                          >
                            <i class="fa-solid fa-trash"></i>
                          </button>
                        </td>
                      </tr>
                    ))}
                  </>
                )}
              </tbody>
            </table>
          </div>
          {/* end table */}
        </div>
      </div>
      {/* modal post */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Tambah Data Siswa</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {" "}
          <form onSubmit={addSiswa} className="px-6">
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Nama Siswa
              </label>
              <input
                placeholder="Nama Siswa"
                onChange={(e) => setNama(e.target.value)}
                value={nama}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                required
              />
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Tempat Lahir{" "}
              </label>
              <input
                placeholder="Tempat Lahir"
                onChange={(e) => setTempatLahir(e.target.value)}
                value={tempatLahir}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                required
              />
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Tanggal Lahir{" "}
              </label>
              <input
                placeholder="Tanggal Lahir"
                type="date"
                onChange={(e) => setTglLahir(e.target.value)}
                value={tglLahir}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                required
              />
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Kelas{" "}
              </label>
              <select
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                onChange={(e) => setKelas(e.target.value)}
              >
                <option selected>Pilih Kelas : </option>
                <option value="VII A">VII A</option>
                <option value="VII B">VII B</option>
                <option value="VII C">VII C</option>
                <option value="VII D">VII D</option>
                <option value="VII E">VII E</option>
                <option value="VII F">VII F</option>
                <option value="VII G">VII G</option>
                <option value="VII H">VII H</option>
                <option value="VII I">VII I</option>
                <option value="VIII A">VIII A</option>
                <option value="VIII B">VIII B</option>
                <option value="VIII C">VIII C</option>
                <option value="VIII D">VIII D</option>
                <option value="VIII E">VIII E</option>
                <option value="VIII F">VIII F</option>
                <option value="VIII G">VIII G</option>
                <option value="VIII H">VIII H</option>
                <option value="VIII I">VIII I</option>
                <option value="IX A">IX A</option>
                <option value="IX B">IX B</option>
                <option value="IX C">IX C</option>
                <option value="IX D">IX D</option>
                <option value="IX E">IX E</option>
                <option value="IX F">IX F</option>
                <option value="IX G">IX G</option>
                <option value="IX H">IX H</option>
                <option value="IX I">IX I</option>
              </select>
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Alamat{" "}
              </label>
              <input
                placeholder="Alamat"
                onChange={(e) => setAlamat(e.target.value)}
                value={alamat}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                required
              />
            </div>
            <div className="flex gap-4 mt-4 ml-[9rem]">
              <button
                onClick={handleClose}
                className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Batal
              </button>
              <button
                type="submit"
                className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Tambah
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      {/* end modal post */}
      {/* modal excel */}
      <Modal show={showExcel} onHide={handleCloseExcel}>
        <Modal.Header closeButton>
          <Modal.Title>Import Siswa Dari File Excel</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="rounded-md text-center shadow-md py-3 border">
            <p>
              Download file di bawah ini untuk menginput data siswa <br />
              jabatan yang dimaksud adalah nama kelas <br />{" "}
              <span className="font-semibold">
                (*column tanggal lahir diubah menjadi date*)
              </span>
            </p>
            <Button variant="primary" onClick={downloadFormatDataSiswa}>
              Download Format File
            </Button>
          </div>
          <form className="mt-4" onChange={importSiswaFromExcel}>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Drop File.xlsx
              </label>
              <input
                type="file"
                required
                accept=".xlsx"
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                onChange={(e) => setExcel(e.target.files[0])}
              />
            </div>
            <div className="flex gap-4 mt-4 ml-[9rem]">
              <button
                onClick={handleCloseExcel}
                className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Batal
              </button>
              <button
                type="submit"
                className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Tambah
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      {/* end modal excel */}
    </>
  );
}
