import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import { useDownloadExcel } from "react-export-table-to-excel";
import Swal from "sweetalert2";

export default function Pasien() {
  const [pasien, setPasien] = useState([]);
  const [dataPasien, setDataPasien] = useState([]);
  const [status, setStatus] = useState("");
  const [keluhan, setKeluhan] = useState("");
  const [data, setData] = useState(0);
  const [show, setShow] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const [rekapData, setRekapData] = useState([]);
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();

  const [downloadData, setDownloadData] = useState([]);

  const [showRekapData, setShowRekapData] = useState(false);
  const [showFilter, setShowFilter] = useState(false);

  const handleCloseFilter = () => setShowFilter(false);
  const handleShowFilter = () => setShowFilter(true);
  const handleShowRekapData = () => setShowRekapData(true);

  const tableRef = useRef(null);

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .get(
        `http://localhost:2929/rekap-data?endDate=${endDate}&startDate=${startDate}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
      .then((res) => {
        setRekapData(res.data.data.informationUks);
        handleShowRekapData();
        handleCloseFilter();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const downloadExcel = async () => {
    const header = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    };
    const response = await axios.get(
      `http://localhost:2929/rekap-data?endDate=${endDate}&startDate=${startDate}`,
      header
    );
    setDownloadData(response.data);
    setTimeout(() => {
      onDownload();
    }, 1000);
  };

  const { onDownload } = useDownloadExcel({
    currentTableRef: tableRef.current,
    filename: "Data Periksa " + new Date(),
    sheet: "Sheet1",
  });

  // const download data
  const download = async (id) => {
    await axios({
      url: `http://localhost:2929/data/api/excel/download/${id}`,
      method: "GET",
      responseType: "blob",
    }).then((response) => {
      setTimeout(() => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");
        fileLink.href = fileURL;
        fileLink.setAttribute("download", "periksa.xlsx");
        document.body.appendChild(fileLink);
        fileLink.click();
      }, 2000);
    });
  };

  // method post
  const addPasien = async (e) => {
    e.preventDefault();
    try {
      await axios.post(
        "http://localhost:2929/pasien",
        {
          data: data,
          keluhan: keluhan,
          status: status,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan Pasien",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };

  // method get all
  const getAllPasien = async () => {
    await axios
      .get("http://localhost:2929/pasien/all-pasien", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setPasien(res.data.data);
      });
  };

  // method get all guru
  const getAllGuru = async () => {
    const guru = await axios.get(
      `http://localhost:2929/data/all-by-status?status=guru`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    setDataPasien(guru.data.data);
  };

  // method get all siswa
  const getAllSiswa = async () => {
    const siswa = await axios.get(
      `http://localhost:2929/data/all-by-status?status=siswa`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    setDataPasien(siswa.data.data);
  };

  // method get all karyawan
  const getAllKaryawan = async () => {
    const karyawan = await axios.get(
      `http://localhost:2929/data/all-by-status?status=karyawan`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    setDataPasien(karyawan.data.data);
  };

  // mathod status for pasien
  const changeStatusPasien = (e) => {
    setStatus(e.target.value);

    if (e.target.value === "siswa") {
      getAllSiswa();
    } else if (e.target.value === "karyawan") {
      getAllKaryawan();
    } else {
      getAllGuru();
    }
  };

  // menampung data
  useEffect(() => {
    getAllPasien();
  }, []);

  console.log(pasien);

  return (
    <>
      {" "}
      <div className="pt-[5rem] pr-[1rem] pb-[3rem] pl-[19rem]">
        {/* card filter tanggal */}
        <div className="shadow-lg rounded-lg my-8 mx-[20px]">
          <div className="flex justify-between items-center bg-[#00dfc4] rounded-t-lg px-5 py-3">
            <h5 className="text-2xl text-white">Filter Rekap Data</h5>
            <Button variant="primary" onClick={handleShowFilter}>
              Filter Tanggal
            </Button>
          </div>
          <div className="py-12">
            {showRekapData ? (
              <div className="grid mb-2 py-6 md:grid-cols-1">
                <p className="text-4xl text-center font-medium">
                  Rekap Data Pasien <br />{" "}
                  <span className="text-xl">
                    {" "}
                    ({startDate}) - ({endDate})
                  </span>
                </p>
                <div className="flex justify-center my-2">
                  <Button
                    className="text-white bg-sky-400 hover:bg-sky-600 focus:ring-4 focus:outline-none focus:ring-sky-300 font-medium rounded-lg text-sm sm:w-auto px-5 py-2.5 text-center w-full"
                    onClick={() => downloadExcel()}
                  >
                    Download Rekap Data Pasien
                  </Button>
                </div>
              </div>
            ) : (
              <h1 className="text-center text-3xl">
                Masukan filter terlebih dahulu <br />{" "}
                <span>sesuai tanggal yang diinginkan</span>
              </h1>
            )}
          </div>
        </div>
        {/* end card filter tanggal */}
        {/* card table pasien */}
        <div className="shadow-lg rounded-lg my-8 mx-[20px]">
          <div className="flex justify-between items-center bg-[#00dfc4] rounded-t-lg px-5 py-3">
            <h5 className="text-2xl text-white">Daftar Pasien</h5>
            <Button variant="primary" onClick={handleShow}>
              Tambah
            </Button>
          </div>
          <div className="pt-3 pb-10 px-3">
            <table className="my-[2rem] w-full">
              <thead className="text-[15px] uppercase text-center bg-gray-200">
                <tr>
                  <th className="px-2 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    No
                  </th>
                  <th className="px-2 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Nama Pasien
                  </th>
                  <th className="px-2 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Status Pasien
                  </th>
                  <th className="px-2 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Jabatan
                  </th>
                  <th className="px-2 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Tanggal & Jam
                  </th>
                  <th className="px-2 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Status{" "}
                  </th>
                  <th className="px-1 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]"></th>
                  <th className="px-2 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                    Aksi
                  </th>
                </tr>
              </thead>
              <tbody className="text-center items-center text-[14px]">
                {pasien.length === 0 ? (
                  <>
                    {" "}
                    <tr>
                      <td
                        colspan="9"
                        className="border-b border-b-gray-200 px-6 py-3 whitespace-nowrap text-base text-center font-medium text-gray-900"
                      >
                        Tidak ada data
                      </td>
                    </tr>
                  </>
                ) : (
                  <>
                    {" "}
                    {pasien.map((pasien1, index) => (
                      <tr
                        key={pasien1.id}
                        className="border-b border-b-gray-200"
                      >
                        <td className="px-2 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {index + 1}
                        </td>
                        <td className="px-2 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {pasien1.data.nama}
                        </td>
                        <td className="px-2 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 capitalize ">
                          {pasien1.data.status}
                        </td>
                        <td className="px-2 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 capitalize">
                          {pasien1.data.jabatan}
                        </td>
                        <td className="px-2 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {pasien1.tglPeriksa}
                        </td>
                        <td className="px-2 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {pasien1.penanganan === "Belum ditangani" ? (
                            <p className="text-red-500 mt-3">Belum Ditangani</p>
                          ) : (
                            <p className="text-green-500 mt-3">
                              Sudah Ditangani
                            </p>
                          )}
                        </td>
                        <td className="px-1 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                          {pasien1.penanganan === "Belum ditangani" ? (
                            <p></p>
                          ) : (
                            <button onClick={() => download(pasien1.id)}>
                              <i class="fa-solid fa-cloud-arrow-down text-[25px]"></i>
                            </button>
                          )}
                        </td>
                        <td className="px-2 whitespace-nowrap font-medium text-center text-xs">
                          {pasien1.penanganan === "Belum ditangani" ? (
                            <a
                              href={"/status-periksa/" + pasien1.id}
                              className="text-xs no-underline"
                            >
                              <p className="text-white mt-3 py-2 rounded-md px-2 bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300">
                                Tangani
                              </p>
                            </a>
                          ) : (
                            <p className="text-white mt-3 ml-2 py-2 rounded-md px-2 bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300">
                              Selesai
                            </p>
                          )}
                        </td>
                      </tr>
                    ))}
                  </>
                )}
              </tbody>
            </table>
          </div>
        </div>
        {/* end card table pasien */}
      </div>
      {/* modal post */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Tambah Pasien</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {" "}
          <form onSubmit={addPasien} className="px-6">
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-44">
                Status Pasien
              </label>
              <select
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                onChange={(e) => changeStatusPasien(e)}
              >
                <option selected>Pilih Status : </option>
                <option value="guru">Guru</option>
                <option value="siswa">Siswa</option>
                <option value="karyawan">Karyawan</option>
              </select>
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-44">
                Nama Pasien
              </label>
              <select
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                onChange={(e) => setData(e.target.value)}
              >
                <option selected>Pilih Nama : </option>
                {dataPasien.map((data1) => (
                  <option value={data1.id}>{data1.nama}</option>
                ))}{" "}
              </select>
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-44">
                Keluhan Pasien
              </label>
              <input
                placeholder="Keluhan Pasien"
                onChange={(e) => setKeluhan(e.target.value)}
                value={keluhan}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-1 border-gray-400 focus:border-[#00c4ad]  focus:ring-4 focus:outline-none focus:ring-[#00dfc4]"
                required
              />
            </div>

            <div className="flex gap-4 mt-4 ml-[9rem]">
              <button
                onClick={handleClose}
                className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Batal
              </button>
              <button
                type="submit"
                className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Tambah
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      {/* end modal post */}
      {/* modal */}
      <Modal show={showFilter} onHide={handleCloseFilter} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Filter Rekap Data Pasien</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit}>
            <div className="name mb-3">
              <Form.Label>
                <strong>Dari Tanggal :</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="2023-09-23 23:09:09"
                  onChange={(e) => setStartDate(e.target.value)}
                />
              </InputGroup>
            </div>
            <div className="name mb-3">
              <Form.Label>
                <strong>Sampai Tanggal :</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="2023-09-23 23:09:09"
                  onChange={(e) => setEndDate(e.target.value)}
                />
              </InputGroup>
            </div>
            <div className="flex gap-4 mt-4 ml-[9rem]">
              <button
                onClick={handleCloseFilter}
                className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-2.5 py-2 text-center"
              >
                Batal
              </button>
              <button
                className="w-full text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-2.5 py-2 text-center"
                type="submit"
              >
                Simpan
              </button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      {/* end modal */}
      {/* table in excel */}
      {showRekapData ? (
        <table
          className="w-full divide-y divide-gray-300 text-center hidden"
          ref={tableRef}
        >
          <thead className="bg-gray-50">
            <tr>
              <th className="px-2 py-2 text-xs text-gray-500">Nama Pasien</th>
              <th className="px-2 py-2 text-xs text-gray-500">Status Pasien</th>
              <th className="px-2 py-2 text-xs text-gray-500">Jabatan</th>
              <th className="px-2 py-2 text-xs text-gray-500">Tanggal</th>
              <th className="px-2 py-2 text-xs text-gray-500">Status</th>
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-300">
            {rekapData.map((periksaa) => (
              <tr key={periksaa.id} className="whitespace-nowrap">
                <td className="px-2 py-3">
                  <div className="text-sm text-gray-900">
                    {periksaa.data.nama}
                  </div>
                </td>
                <td className="px-2 py-3">
                  <div className="text-sm text-gray-500">{periksaa.status}</div>
                </td>
                <td className="px-2 py-3 text-sm text-gray-500">
                  {periksaa.data.jabatan}
                </td>
                <td className="px-2 py-3">{periksaa.tglPeriksa}</td>
                <td className="px-2 py-3">
                  <div
                    className={
                      periksaa.penanganan
                        ? `text-sm text-green-500`
                        : "text-sm text-red-500"
                    }
                  >
                    {periksaa.penanganan
                      ? "Sudah ditangani"
                      : "Belum ditangani"}
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      ) : null}
      {/* end table excel */}
    </>
  );
}
