import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Login from "./component/Login";
import Sidebar from "./component/Sidebar";
import Home from "./page/Home";
import Diagnosa from "./page/Diagnosa";
import Navbar from "./component/Navbar";
import Obat from "./page/Obat";
import Tindakan from "./page/Tindakan";
import Penanganan from "./page/Penanganan";
import Dashboard from "./component/Dashboard";
import Guru from "./page/Guru";
import Siswa from "./page/Siswa";
import Karyawan from "./page/Karyawan";
import PutObat from "./editpage/PutObat";
import PutTindakan from "./editpage/PutTindakan";
import PutPenanganan from "./editpage/PutPenanganan";
import PutDiagnosa from "./editpage/PutDiagnosa";
import PutSiswa from "./editpage/PutSiswa";
import PutGuru from "./editpage/PutGuru";
import PutKaryawan from "./editpage/PutKaryawan";
import Pasien from "./page/Pasien";
import Register from "./component/Register";
import Profile from "./component/Profile";
import StatusPasien from "./page/StatusPasien";
import PutProfile from "./editpage/PutProfile";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route path="/" component={Login} exact />
          <Route path="/register" component={Register} exact />
          {localStorage.getItem("token") === null ? (
            <>
              {" "}
              <PrivateRoute path="/home" component={Home} exact />
              <PrivateRoute path="/editObat/:id" component={PutObat} exact />
              <PrivateRoute
                path="/editTindakan/:id"
                component={PutTindakan}
                exact
              />
              <PrivateRoute
                path="/editPenanganan/:id"
                component={PutPenanganan}
                exact
              />
              <PrivateRoute
                path="/editDiagnosa/:id"
                component={PutDiagnosa}
                exact
              />
              <PrivateRoute path="/editSiswa/:id" component={PutSiswa} exact />
              <PrivateRoute path="/editGuru/:id" component={PutGuru} exact />
              <PrivateRoute
                path="/editKaryawan/:id"
                component={PutKaryawan}
                exact
              />
              <PrivateRoute
                path="/editProfile/:id"
                component={PutProfile}
                exact
              />
              <div>
                <Sidebar />
                <Navbar />
                <PrivateRoute path="/dashboard" component={Dashboard} exact />
                <PrivateRoute path="/diagnosa" component={Diagnosa} exact />
                <PrivateRoute path="/obat" component={Obat} exact />
                <PrivateRoute path="/tindakan" component={Tindakan} exact />
                <PrivateRoute path="/penanganan" component={Penanganan} exact />
                <PrivateRoute path="/guru" component={Guru} exact />
                <PrivateRoute path="/siswa" component={Siswa} exact />
                <PrivateRoute path="/karyawan" component={Karyawan} exact />
                <PrivateRoute path="/periksa-pasien" component={Pasien} exact />
                <PrivateRoute
                  path="/status-periksa/:id"
                  component={StatusPasien}
                  exact
                />
                <PrivateRoute path="/profile" component={Profile} exact />
              </div>
            </>
          ) : (
            <Switch>
              {" "}
              <Route path="/home" component={Home} exact />
              <Route path="/editObat/:id" component={PutObat} exact />
              <Route path="/editTindakan/:id" component={PutTindakan} exact />
              <Route
                path="/editPenanganan/:id"
                component={PutPenanganan}
                exact
              />
              <Route path="/editDiagnosa/:id" component={PutDiagnosa} exact />
              <Route path="/editSiswa/:id" component={PutSiswa} exact />
              <Route path="/editGuru/:id" component={PutGuru} exact />
              <Route path="/editKaryawan/:id" component={PutKaryawan} exact />
              <Route path="/editProfile/:id" component={PutProfile} exact />
              <div>
                <Sidebar />
                <Navbar />
                <Route path="/dashboard" component={Dashboard} exact />
                <Route path="/diagnosa" component={Diagnosa} exact />
                <Route path="/obat" component={Obat} exact />
                <Route path="/tindakan" component={Tindakan} exact />
                <Route path="/penanganan" component={Penanganan} exact />
                <Route path="/guru" component={Guru} exact />
                <Route path="/siswa" component={Siswa} exact />
                <Route path="/karyawan" component={Karyawan} exact />
                <Route path="/periksa-pasien" component={Pasien} exact />
                <Route
                  path="/status-periksa/:id"
                  component={StatusPasien}
                  exact
                />
                <Route path="/profile" component={Profile} exact />
              </div>
            </Switch>
          )}
        </Switch>
      </BrowserRouter>
    </div>
  );
}

// private router
function PrivateRoute({ component: Component, isAuthenticated, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
      }
    />
  );
}

export default App;
