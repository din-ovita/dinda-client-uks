import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function PutDiagnosa() {
  const [namaDiagnosa, setNamaDiagnosa] = useState("");
  // untuk mengambil id
  const param = useParams();
  const history = useHistory();
  // fungsi put / update
  const put = async (e) => {
    e.preventDefault();
    try {
      await axios.put(`http://localhost:2929/diagnosa/` + param.id, {
        namaDiagnosa: namaDiagnosa,
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/diagnosa");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  // untuk menampung data per id
  useEffect(() => {
    axios
      .get("http://localhost:2929/diagnosa/" + param.id)
      .then((response) => {
        const diagnosa = response.data.data;
        setNamaDiagnosa(diagnosa.namaDiagnosa);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);

  return (
    <div className="min-h-screen flex justify-center items-center">
      <div className="rounded-md shadow-lg bg-white px-16 py-12">
        {/* header */}
        <h3 className="bg-[#00dfc4] text-white text-center py-2 rounded-l-full rounded-r-full">
          Edit Diagnosa
        </h3>
        {/* end header */}
        {/* form edit */}
        <form onSubmit={put}>
          {" "}
          <div className="mb-3">
            <label className="text-base font-semibold text-[#1d2b3a] my-2">
              Nama Penyakit
            </label>
            <input
              placeholder="Nama Penyakit"
              onChange={(e) => setNamaDiagnosa(e.target.value)}
              value={namaDiagnosa}
              className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-2 border-2 border-gray-100 focus:outline-[#00dfc4]"
              required
            />
          </div>
          <div className="flex gap-3 mt-4 ml-[9rem]">
            <a
              href="/diagnosa"
              className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 text-center no-underline"
            >
              Batal
            </a>
            <button
              type="submit"
              className="w-full text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
            >
              Simpan
            </button>
          </div>
        </form>
        {/* end form edit */}
      </div>
    </div>
  );
}
