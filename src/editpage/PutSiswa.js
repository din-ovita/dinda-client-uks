import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function PutSiswa() {
  const [nama, setNama] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [tglLahir, setTglLahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const [kelas, setKelas] = useState("");
  // untuk mengambil id
  const param = useParams();
    const history = useHistory();
  // const put atau update
  const put = async (e) => {
    e.preventDefault();

    const data = new FormData();
    data.append("nama", nama);
    data.append("alamat", alamat);
    data.append("tempatLahir", tempatLahir);
    data.append("tglLahir", tglLahir);
    data.append("status", "siswa");
    data.append("jabatan", kelas);

    try {
      await axios.put(`http://localhost:2929/data/` + param.id, data);
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/siswa");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  // untuk menampung data per id
  useEffect(() => {
    axios
      .get("http://localhost:2929/data/" + param.id)
      .then((response) => {
        const data1 = response.data.data;
        setNama(data1.nama);
        setAlamat(data1.alamat);
        setKelas(data1.kelas);
        setTempatLahir(data1.tempatLahir);
        setTglLahir(data1.tglLahir);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);

  return (
    <div className="min-h-screen flex justify-center items-center">
      <div className="rounded-md shadow-lg bg-white px-16 py-4">
        {/* header */}
        <h3 className="bg-[#00dfc4] text-white text-center py-2 rounded-l-full rounded-r-full">
          Edit Data Siswa
        </h3>
        {/* end header */}
        {/* form edit */}
        <form onSubmit={put}>
          {" "}
          <div className="mb-2">
            <label className="text-base font-semibold text-[#1d2b3a] my-1">
              Nama Siswa
            </label>
            <input
              placeholder="Nama Siswa"
              onChange={(e) => setNama(e.target.value)}
              value={nama}
              className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-2 border-2 border-gray-100 focus:outline-[#00dfc4]"
              required
            />
          </div>
          <div className="mb-2">
            <label className="text-base font-semibold text-[#1d2b3a] my-1">
              Kelas{" "}
            </label>
            <select
              className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-2 border-2 border-gray-100 focus:outline-[#00dfc4]"
              onChange={(e) => setKelas(e.target.value)}
            >
              <option selected>Pilih Kelas : </option>
              <option value="VII A">VII A</option>
              <option value="VII B">VII B</option>
              <option value="VII C">VII C</option>
              <option value="VII D">VII D</option>
              <option value="VII E">VII E</option>
              <option value="VII F">VII F</option>
              <option value="VII G">VII G</option>
              <option value="VII H">VII H</option>
              <option value="VII I">VII I</option>
              <option value="VIII A">VIII A</option>
              <option value="VIII B">VIII B</option>
              <option value="VIII C">VIII C</option>
              <option value="VIII D">VIII D</option>
              <option value="VIII E">VIII E</option>
              <option value="VIII F">VIII F</option>
              <option value="VIII G">VIII G</option>
              <option value="VIII H">VIII H</option>
              <option value="VIII I">VIII I</option>
              <option value="IX A">IX A</option>
              <option value="IX B">IX B</option>
              <option value="IX C">IX C</option>
              <option value="IX D">IX D</option>
              <option value="IX E">IX E</option>
              <option value="IX F">IX F</option>
              <option value="IX G">IX G</option>
              <option value="IX H">IX H</option>
              <option value="IX I">IX I</option>
            </select>
          </div>
          <div className="mb-2">
            <label className="text-base font-semibold text-[#1d2b3a] my-1">
              Tempat Lahir
            </label>
            <input
              placeholder="Tempat Lahir"
              onChange={(e) => setTempatLahir(e.target.value)}
              value={tempatLahir}
              className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-2 border-2 border-gray-100 focus:outline-[#00dfc4]"
              required
            />
          </div>
          <div className="mb-2">
            <label className="text-base font-semibold text-[#1d2b3a] my-1">
              Tanggal Lahir
            </label>
            <input
              onChange={(e) => setTglLahir(e.target.value)}
              value={tglLahir}
              type="date"
              className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-2 border-2 border-gray-100 focus:outline-[#00dfc4]"
              required
            />
          </div>
          <div className="mb-2">
            <label className="text-base font-semibold text-[#1d2b3a] my-1">
              Alamat
            </label>
            <input
              placeholder="Alamat"
              onChange={(e) => setAlamat(e.target.value)}
              value={alamat}
              className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-2 border-2 border-gray-100 focus:outline-[#00dfc4]"
              required
            />
          </div>
          <div className="flex gap-3 mt-4 ml-[9rem]">
            <a
              href="/siswa"
              className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 text-center no-underline"
            >
              Batal
            </a>
            <button
              type="submit"
              className="w-full text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
            >
              Simpan
            </button>
          </div>
        </form>
        {/* end form */}
      </div>
    </div>
  );
}
