import axios from "axios";
import React, { useEffect, useState } from "react";

function Navbar() {
  const [akun, setAkun] = useState([]);

  // get akun by id
  const getAkun = async () => {
    await axios
      .get("http://localhost:2929/akun/" + localStorage.getItem("userId"))
      .then((res) => {
        setAkun(res.data.data);
      });
  };

  // useEfect unutuk menampung data
  useEffect(() => {
    getAkun();
  }, []);

  return (
    <div className="header-web ml-[18rem] flex justify-between items-center fixed bg-white w-[79%] shadow-md">
      {/* logo */}
      <a href="/home" className="cursor-pointer no-underline text-[#292929]">
        <div className="flex justify-center items-center mx-4">
          <img
            src="http://103.133.27.106:3000/static/media/smpn1smg.174af4089a0f0045f9a4.png"
            className="w-[50px] h-[50px]"
          />
          <span className="text-xl font-medium pl-3">SMP N 1 SEMARANG</span>
        </div>
      </a>
      {/* end logo */}
      {/* profille */}
      <div className="profile bg-[#00dfc4] rounded-bl-full py-[10px] pl-[3rem] pr-[2rem]">
        <a href="/profile">
          {akun.foto == null ? (
            <>
              {/* validasi jika foto profile tidak ada */}
              <img
                src="https://www.its.ac.id/international/wp-content/uploads/sites/66/2020/02/blank-profile-picture-973460_1280-300x300.jpg"
                className="w-[50px] h-50px rounded-full"
              />
            </>
          ) : (
            <>
              <img src={akun.foto} className="w-[50px] h-50px rounded-full" />
            </>
          )}
        </a>
      </div>
      {/* end profile */}
    </div>
  );
}

export default Navbar;
