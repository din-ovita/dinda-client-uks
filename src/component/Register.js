import axios from "axios";
import React from "react";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../csscomponent/login.css";

export default function Register() {
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

    const history = useHistory();
  // post akun
  const addAkun = async (e) => {
    e.preventDefault();
    try {
      await axios
        .post("http://localhost:2929/akun/register", {
          email: email,
          username: username,
          password: password,
        })
        .then(() => {
          // sweetalert success
          Swal.fire({
            icon: "success",
            title: "Berhasil Registrasi",
            showConfirmButton: false,
            timer: 1500,
          });
          setTimeout(() => {
            history.push("/");
            window.location.reload();
          }, 1500);
        });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <div className="flex justify-center items-center flex-col min-h-screen">
        <div className="">
          <h1 className="text-[2.15em] font-semibold">Sistem Aplikasi UKS</h1>
          <img
            src="http://103.133.27.106:3000/static/media/smpn1smg.174af4089a0f0045f9a4.png"
            className="h-[9.5rem] m-auto"
          />
        </div>
        {/* form post */}
        <form
          onSubmit={addAkun}
          className="border-slate-100 px-16 py-8 rounded-md shadow-lg mt-2"
        >
          <h4 className="text-center">DAFTAR</h4>
          <div className="inputBox">
            <input
              type="text"
              required="required"
              onChange={(e) => setEmail(e.target.value)}
            />
            <span>Email</span>
          </div>
          <div className="inputBox">
            <input
              type="text"
              required="required"
              onChange={(e) => setUsername(e.target.value)}
            />
            <span>Username</span>
          </div>
          <div className="inputBox">
            <input
              type="password"
              required="required"
              onChange={(e) => setPassword(e.target.value)}
            />
            <span>Password</span>
          </div>
          <div className="button">
            <button type="submit">Daftar</button>
          </div>
          {/* link menuju page login */}
          <p className="text-[0.9em] pt-2 text-center">
            Sudah punya akun? <a href="/">Masuk</a>
          </p>
        </form>
        {/* end form */}
      </div>
    </div>
  );
}
