import React, { useEffect, useState } from "react";
import "../csscomponent/sidebar.css";
import {useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function Sidebar() {
    const history = useHistory();
  // validasi untuk membuka menu sidebar
  const [open, setOpen] = useState(false);

  // fungsi logout
  const logout = () => {
    Swal.fire({
      title: "Anda Yakin Ingin Logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          title: "Berhasil Logout",
          showConfirmButton: false,
          timer: 1500,
        });
        history.push("/");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        localStorage.clear();
      }
    });
  };

  // fungsi date (tanggal)
  const [date, setDate] = useState(new Date());

  // useEffect untuk menampung data
  useEffect(() => {
    // fungsi tiime
    const timer = setInterval(() => setDate(new Date()), 1000);
    return function cleanup() {
      clearInterval(timer);
    };
  }, []);

  return (
    // sidebar
    <div className="sidebar hidden">
      <h5>
        SISTEM APLIKASI UKS <br />
      </h5>
      <ul>
        {/* menu dashboard */}
        <a href="/dashboard">
          <li>
            <i class="fa-solid fa-palette"></i> <span>Dashboard</span>
          </li>
        </a>
        {/* menu periksa pasien */}
        <a href="/periksa-pasien">
          <li>
            <i class="fa-solid fa-stethoscope"></i> <span>Periksa Pasien</span>
          </li>
        </a>
        {/* menu data */}
        <details className="group [&_summary::-webkit-details-marker]:hidden">
          <summary className="flex cursor-pointer items-center justify-between px-[28px] py-[10px] hover:bg-[#00dfc4] hover:text-[#292929]">
            <div className="flex items-center gap-2">
              <i class="fa-solid fa-server text-center w-[30px]"></i>{" "}
              <span className={`text-[1.09em] ${open && "hidden"}`}>Data</span>
            </div>

            <span
              className={`shrink-0 transition duration-300 group-open:-rotate-90 ${
                open && "hidden"
              }`}
            >
              <i className="fa-solid fa-caret-left"></i>{" "}
            </span>
          </summary>

          {/* sub menu data guru */}
          <a
            href="/guru"
            className="flex cursor-pointer items-center px-[50px] py-[10px] hover:bg-[#00dfc4] hover:text-[#292929]"
          >
            <i class="fa-solid fa-user-tie text-center w-[30px]"></i>{" "}
            <span
              className={`text-[0.9em] text-center mt-1 ${open && "hidden"}`}
            >
              Data Guru
            </span>
          </a>
          {/* sub menu data siswa */}
          <a
            href="/siswa"
            className="flex cursor-pointer items-center px-[50px] py-[10px] hover:bg-[#00dfc4] hover:text-[#292929]"
          >
            <i class="fa-solid fa-user text-center w-[30px]"></i>{" "}
            <span
              className={`text-[0.9em] text-center mt-1 ${open && "hidden"}`}
            >
              Data Siswa
            </span>
          </a>
          {/* sub menu data karyawan */}
          <a
            href="/karyawan"
            className="flex cursor-pointer items-center px-[50px] py-[10px] hover:bg-[#00dfc4] hover:text-[#292929]"
          >
            <i class="fa-solid fa-users text-center w-[30px]"></i>{" "}
            <span
              className={`text-[0.9em] text-center mt-1 ${open && "hidden"}`}
            >
              Data Karyawan
            </span>
          </a>
        </details>
        {/* menu diagnosa */}
        <a href="/diagnosa">
          <li>
            <i class="fa-solid fa-person-dots-from-line"></i>{" "}
            <span>Diagnosa</span>
          </li>
        </a>
        {/* menu penanganan */}
        <a href="/penanganan">
          <li>
            <i class="fa-solid fa-hands-holding-child"></i>{" "}
            <span>Penanganan Pertama</span>
          </li>
        </a>
        {/* menu tindakan */}
        <a href="/tindakan">
          <li>
            <i class="fa-solid fa-location-crosshairs"></i>{" "}
            <span>Tindakan</span>
          </li>
        </a>
        {/* menu obat */}
        <a href="/obat">
          <li>
            <i class="fa-solid fa-capsules"></i> <span>Obat P3K</span>
          </li>
        </a>
      </ul>
      {/* tanggal dan jam */}
      <div className="mt-12 block items-center justify-center leading-[2rem] text-lg text-[0.9em]">
        <span>{date.getDate()}</span>
        <span>/{date.getMonth() + 1}</span>
        <span>/{date.getFullYear()}</span>
      </div>
      <p className="text-[1.3em] mt-2">{date.toLocaleTimeString()}</p>
      {/* button logout */}
      <button onClick={logout}>
        <i class="fa-solid fa-right-from-bracket"></i> Keluar
      </button>
    </div>
  );
}
