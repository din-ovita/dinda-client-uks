import axios from "axios";
import React, { useEffect, useState } from "react";

export default function Dashboard() {
  const [guru, setGuru] = useState([]);
  const [siswa, setSiswa] = useState([]);
  const [karyawan, setKaryawan] = useState([]);
  const [pasien, setPasien] = useState([]);

  // get all pasien guru
  const allPasienGuru = async () => {
    await axios
      .get("http://localhost:2929/pasien/all-by-status-pasien?status=guru")
      .then((ress) => {
        setGuru(ress.data.data);
      });
  };

  // get all pasien siswa
  const allPasienSiswa = async () => {
    await axios
      .get("http://localhost:2929/pasien/all-by-status-pasien?status=siswa")
      .then((ress) => {
        setSiswa(ress.data.data);
      });
  };

  // get all pasien karyawan
  const allPasienKaryawan = async () => {
    await axios
      .get("http://localhost:2929/pasien/all-by-status-pasien?status=karyawan")
      .then((ress) => {
        setKaryawan(ress.data.data);
      });
  };

  // get all pasien
  const allPasien = async (e) => {
    await axios.get("http://localhost:2929/pasien/all-pasien").then((ress) => {
      setPasien(ress.data.data);
    });
  };

  // useEffect untuk menampung data
  useEffect(() => {
    allPasienGuru();
    allPasienSiswa();
    allPasienKaryawan();
    allPasien();
  }, []);

  return (
    <div className="pt-[5rem] pr-[1rem] pb-[3rem] pl-[19rem]">
      {/* card */}
      <div className="flex justify-center items-center gap-10 pt-3">
        {/* card pasien guru */}
        <div className="shadow-lg py-3 px-16 rounded-md text-center">
          <h2 className="text-lg font-semibold">Daftar Pasien Guru</h2>
          <i class="fa-solid fa-wheelchair bg-[#00dfc4] text-white text-4xl py-2 px-2 rounded-full"></i>{" "}
          <span className="text-3xl font-semibold text-[#00dfc4] pl-2">
            {guru.length} Guru
          </span>
        </div>
        {/* card pasien siswa */}
        <div className="shadow-lg py-3 px-16 rounded-md text-center">
          <h2 className="text-lg font-semibold">Daftar Pasien Siswa</h2>
          <i class="fa-solid fa-wheelchair bg-[#00dfc4] text-white text-4xl py-2 px-2 rounded-full"></i>{" "}
          <span className="text-3xl font-semibold text-[#00dfc4] pl-2">
            {siswa.length} Siswa
          </span>
        </div>
        {/* card pasien karyawan */}
        <div className="shadow-lg py-3 px-12 rounded-md text-center">
          <h2 className="text-lg font-semibold">Daftar Pasien Karyawan</h2>
          <i class="fa-solid fa-wheelchair bg-[#00dfc4] text-white text-4xl py-2 px-2 rounded-full"></i>{" "}
          <span className="text-3xl font-semibold text-[#00dfc4] pl-2">
            {karyawan.length} Karyawan
          </span>
        </div>
      </div>
      {/* end card */}
      {/* table */}
      <div className="shadow-lg my-16 mx-[20px] rounded-lg">
        <h2 className="text-center text-2xl py-3 bg-[#00dfc4] rounded-t-lg text-white ">
          Riwayat Pasien
        </h2>
        <div className="pt-3 pb-10 px-5">
          <table className="w-full">
            <thead className="text-[15px] uppercase text-center bg-gray-200">
              <tr>
                <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                  No
                </th>
                <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                  Nama Pasien
                </th>
                <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                  Status Pasien
                </th>
                <th className="px-6 py-3 whitespace-nowrap md:text-sm text-xs font-semibold text-[#1d2b3a]">
                  Tanggal & Jam Periksa
                </th>
              </tr>
            </thead>
            <tbody className="text-center text-[14px]">
              {pasien.length === 0 ? (
                <>
                  <tr>
                    <td
                      colspan="4"
                      className="border-b border-b-gray-200 px-6 py-3 whitespace-nowrap text-base text-center font-medium text-gray-900"
                    >
                      Tidak ada data
                    </td>
                  </tr>
                </>
              ) : (
                <>
                  {" "}
                  {pasien.map((pasien1, index) => (
                    <tr className="border-b border-b-gray-200">
                      <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                        {index + 1}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                        {pasien1.data.nama}
                      </td>
                      <td className="capitalize px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                        {pasien1.status}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900">
                        {pasien1.tglPeriksa}
                      </td>
                    </tr>
                  ))}
                </>
              )}
            </tbody>
          </table>
        </div>
      </div>
      {/* end table */}
    </div>
  );
}
