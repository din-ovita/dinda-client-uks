import axios from "axios";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import bcrypt from "bcryptjs";
import { Modal } from "react-bootstrap";

export default function Profile() {
  const [akun, setAkun] = useState([]);
  const [passwordType, setPasswordType] = useState("password");
  const [passwordType1, setPasswordType1] = useState("password");
  const [passwordType2, setPasswordType2] = useState("password");
  const [passLama, setPassLama] = useState("");
  const [conPassLama, setConPassLama] = useState("");
  const [passBaru, setPassBaru] = useState("");
  const [conPassBaru, setConPassBaru] = useState("");

  const [show, setShow] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  // get akun by id
  const getAkun = async () => {
    await axios
      .get("http://localhost:2929/akun/" + localStorage.getItem("userId"))
      .then((res) => {
        setAkun(res.data.data);
      });
  };

  // useEffect untuk menampung data
  useEffect(() => {
    getAkun();
    conPassword();
  }, []);

  const conPassword = async () => {
    await axios
      .get("http://localhost:2929/akun/" + localStorage.getItem("userId"))
      .then((res) => {
        setPassLama(res.data.data.password);
      });
  };

  const ubahPassword = (e) => {
    e.preventDefault();
    bcrypt.compare(conPassLama, passLama, function (err, isMatch) {
      if (err) {
        throw err;
      } else if (!isMatch) {
        Swal.fire({
          icon: "error",
          title: "Password tidak sama dengan yang sebelumnya!",
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        if (passBaru === conPassLama) {
          Swal.fire({
            icon: "error",
            title: "Password tidak boleh sama dengan sebelumnya!",
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          if (passBaru === conPassBaru) {
            axios
              .put(
                "http://localhost:2929/akun/password/" +
                  localStorage.getItem("userId"),
                {
                  password: passBaru,
                }
              )
              .then(() => {
                setShow(false);
                Swal.fire({
                  icon: "success",
                  title: " Berhasil!!!",
                  showConfirmButton: false,
                  timer: 1500,
                });
                setTimeout(() => {
                  window.location.reload();
                }, 1500);
              })
              .catch((err) => {
                Swal.fire({
                  icon: "error",
                  title:
                    "Password minimal 8-20 karater dan menggunakan huruf kecil",
                  showConfirmButton: false,
                  timer: 1500,
                });
                console.log(err);
              });
          } else {
            Swal.fire({
              icon: "error",
              title: "Password Tidak Sama",
              showConfirmButton: false,
              timer: 1500,
            });
          }
        }
      }
    });
  };

  return (
    <>
      <div className="pt-[8rem] pr-[3rem] pb-[3rem] pl-[21rem]">
        <div className="shadow-md rounded-md">
          <div className="w-full h-28 rounded-t-md bg-gradient-to-r from-[#00dfc4] to-[#00c4ad]"></div>
          <div className="absolute top-[12rem] left-[23rem]">
            {/* validasi jika foto profile tidak ada */}
            {akun.foto == null ? (
              <>
                <img
                  src="https://www.its.ac.id/international/wp-content/uploads/sites/66/2020/02/blank-profile-picture-973460_1280-300x300.jpg"
                  className="w-[150px] h-[150px] rounded-full border-4 border-white"
                />
              </>
            ) : (
              <>
                <img
                  src={akun.foto}
                  className="w-[150px] h-[150px] rounded-full border-4 border-white"
                />
              </>
            )}
          </div>
          <div className="px-10 pl-[15rem] pt-12 pb-6">
            <h1 className="text-2xl font-semibold">@{akun.username}</h1>
            <h1 className="text-base text-gray-400">{akun.email}</h1>
          </div>
          <div className="flex justify-end gap-4 px-9 pb-8">
            <button className="bg-[#00dfc4] rounded-md hover:bg-[#00c4ad] py-2 px-3">
              <a
                href={"/editProfile/" + akun.id}
                className="no-underline text-white"
              >
                Edit Profile
              </a>
            </button>
            <button
              onClick={handleShow}
              className=" border-2 border-[#00dfc4] rounded-md hover:border-[#00c4ad] py-2 px-3 text-[#00c4ad] hover:text-[#00dfc4]"
            >
              Edit Password
            </button>
          </div>
          <hr />
          {/* footer */}
          <div className="py-1">
            <p className="text-center leading-tight text-gray-500">
              Kelola informasi pribadi Anda.
              <br /> <span> Untuk melidungi dan mengamankan akun.</span>
            </p>
          </div>
          {/* end footer */}
        </div>
      </div>
      {/* modal edit */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {" "}
          <form onSubmit={ubahPassword} className="px-6">
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-44">
                Password Lama
              </label>
              <input
                required
                placeholder="Masukan password lama anda"
                type={passwordType2}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-2 border-[#292929]"
                onChange={(e) => setConPassLama(e.target.value)}
              />
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-44">
                Password Baru
              </label>
              <input
                required
                placeholder="Masukan password baru"
                type={passwordType}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-2 border-[#292929]"
                onChange={(e) => setPassBaru(e.target.value)}
              />
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-44">
                Konfirmasi Password Baru{" "}
              </label>
              <input
                placeholder="Konfirmasi Password Baru"
                type={passwordType1}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-2 border-[#292929]"
                onChange={(e) => setConPassBaru(e.target.value)}
              />
            </div>

            <div className="flex gap-4 mt-4 ml-[9rem]">
              <button
                onClick={handleClose}
                className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Batal
              </button>
              <button
                type="submit"
                className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Ubah
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      {/* end modal edit */}
    </>
  );
}
