import React, { useState } from "react";
import "../csscomponent/login.css";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

    const history = useHistory();
  // validasi login
  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:2929/akun/login",
        {
          email: email,
          password: password,
        }
      );
      // sweetalert jika kondisi status 200
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasil!!!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.data.user.id);
        localStorage.setItem("token", data.data.token);
        history.push("/home");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    } catch (error) {
      // sweetalert error
      Swal.fire({
        icon: "error",
        title: "Email atau password salah!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  return (
    <div>
      <div className="body">
        <div className="header">
          <h1>Sistem Aplikasi UKS</h1>
          <img src="http://103.133.27.106:3000/static/media/smpn1smg.174af4089a0f0045f9a4.png" />
        </div>
        {/* form login */}
        <form
          onSubmit={login}
          className="border-slate-100 px-12 py-8 rounded-md shadow-lg"
        >
          <h4>MASUK</h4>
          <div className="inputBox">
            <input
              type="text"
              required="required"
              onChange={(e) => setEmail(e.target.value)}
            />
            <span>Email / Username</span>
          </div>
          <div className="inputBox">
            <input
              type="password"
              required="required"
              onChange={(e) => setPassword(e.target.value)}
            />
            <span>Password</span>
          </div>
          <div className="button">
            <button type="submit">Masuk</button>
          </div>
        </form>
        {/* end form */}
      </div>
    </div>
  );
}

export default Login;
